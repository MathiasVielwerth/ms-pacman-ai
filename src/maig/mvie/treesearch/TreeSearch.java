/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maig.mvie.treesearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import pacman.game.Constants.GHOST;
import pacman.game.Game;

/**
 *
 * @author mvie
 */
public class TreeSearch {
    private TreeNode _root;
    private Game _game;
    private int _maxDepth;

    private int _lastBest;
    
    private ArrayList<TreeNode> leafs;
    private TreeMap<GHOST, Integer> dangerousGhosts = new TreeMap<GHOST, Integer>();
    private ArrayList<Integer> edibleGhosts = new ArrayList<Integer>();
    
    public TreeSearch(Game game, int maxDepth, int lastBest) {
        _game = game;
        _maxDepth = maxDepth;
        _lastBest = lastBest;

        int pacPos = _game.getPacmanCurrentNodeIndex();
        for(GHOST g : GHOST.values()) {
            if(_game.getGhostLairTime(g) != 0)
                continue;

            int ghostPos = _game.getGhostCurrentNodeIndex(g);

            boolean isEdible = _game.getGhostEdibleTime(g) > _game.getShortestPathDistance(pacPos, ghostPos);
            int pos = _game.getGhostCurrentNodeIndex(g);
            if(!isEdible) {
                dangerousGhosts.put(g, pos);
                //System.out.println(g + " is on " +_game.getGhostCurrentNodeIndex(g));
            }
            if(isEdible) {
                edibleGhosts.add(pos);
            }
        }
    }
    
    public void search(int position) {
        leafs = new ArrayList<TreeNode>();
        _root = new TreeNode(position, null);
        search(position, _root, 0, new ArrayList<Integer>());
    }
    
    private TreeNode search(int position, TreeNode prevNode, int depth, ArrayList<Integer> visitedPills) {
        int[] nextNodes = _game.getNeighbouringNodes(position);
        
        TreeNode currentNode = new TreeNode(position, prevNode);

        int powerPillIndex = _game.getPowerPillIndex(position);
        boolean isPowerPill = powerPillIndex != -1;
        boolean isPill = isPillAvailable(visitedPills, position);
        float pacDist = _game.getShortestPath(_game.getPacmanCurrentNodeIndex(), position).length + 0.01f;
        float dangerGhostDist = distToClosestGhost(position, dangerousGhosts);
        //float edibleGhostDist = distToClosestGhost(position, edibleGhosts);
        
        // FIND POINTS
        if (isPill) {
            //currentNode.addPoints(10 * 1f/pacDist + (100 * 1f/_game.getNumberOfActivePills()));
            currentNode.addPoints(30 * 1f / depth);
            visitedPills.add(_game.getPillIndex(position));
        }

        if (position == _lastBest && depth < 3) {
            currentNode.addPoints(300);
        }

        if (isPowerPill && _game.isPowerPillStillAvailable(powerPillIndex)) {
            currentNode.addPoints(-500 * edibleTimeLeft());
            if(!isAllGhostOut()) currentNode.addPoints(-500);
            else currentNode.addPoints(500);
        }

//        if(edibleGhostDist < 5) {
//            currentNode.addPoints(50 * 1f/edibleGhostDist);
//        }
        currentNode.setPillCount(visitedPills.size());

        // STOP?
        if(dangerGhostDist < depth + 10) {
            currentNode.addPoints(-500 * 1f/dangerGhostDist);
            //currentNode.addPoints(-100);
            leafs.add(currentNode);
            return currentNode;
        }

        if(depth >= _maxDepth) {
            leafs.add(currentNode);
            return currentNode;
        }

        
        // CREATE CHILDREN
        for(int nextPos : nextNodes) {
            if(nextPos == prevNode.getIndex() && !isPill)
                continue;
            currentNode.addChild(search(nextPos, currentNode, depth +1, (ArrayList<Integer>)visitedPills.clone()));
        }
        
        // RETURN NODE
        return currentNode;
    }

    private boolean isPillAvailable(ArrayList<Integer> visitedPills, int position) {
        int pillIndex = _game.getPillIndex(position);
        boolean isPill = pillIndex != -1;

        return isPill && !visitedPills.contains(pillIndex) && _game.isPillStillAvailable(pillIndex);
    }

    private float edibleTimeLeft() {
        float time = Float.MAX_VALUE;
        for(GHOST g : GHOST.values()) {
            time = Math.min(time, _game.getGhostEdibleTime(g));
        }
        return time;
    }

    private boolean isAllGhostOut() {
        int time = 0;
        for(GHOST g : GHOST.values()) {
            time = Math.max(time, _game.getGhostLairTime(g));
        }
        return time == 0;
    }
    
    private float distToClosestGhost(int position, TreeMap<GHOST, Integer> ghostType) {
        int dist = Integer.MAX_VALUE;
        for(Map.Entry<GHOST, Integer> kv : ghostType.entrySet()) {
            dist = Math.min(dist, _game.getShortestPath(kv.getValue(), position, _game.getGhostLastMoveMade(kv.getKey())).length);
        }
        return dist + 0.01f;
    }
    
    public ArrayList<Integer> getRichestPath() {
        if(leafs == null) 
            throw new IllegalStateException("Search have not yet been conducted");
        
        TreeNode bestNode = null;
        float bestScore = Integer.MIN_VALUE;
        boolean allZero = true;
        for(TreeNode node : leafs) {
            //System.out.println("Leaf score " + node.getPoints() + " on node " + node);
            if(node.getPoints() > bestScore) {
                bestScore = node.getPoints();
                bestNode = node;
                allZero &= node.getPillCount() == 0 && node.getPoints() >= 0;
            }
        }

        //System.out.println("Best node " + bestNode + " with score " + bestScore);
        
        return allZero ? null : bestNode.getPath();
    }
}

class TreeNode {
    private int _index;
    private float _points = 0;
    private TreeNode _parent;
    private int pillCount;
    private ArrayList<TreeNode> children = new ArrayList<TreeNode>();
    public TreeNode(int index, TreeNode parent) {
        _index = index;
        _parent = parent;
        if(parent != null)
            _points = parent._points;
    }

    void setPillCount(int pills) {
        pillCount = pills;
    }
    public int getPillCount() {
        return pillCount;
    }
    
    public void addChild(TreeNode child) {
        children.add(child);
    }
    
    public void addPoints(float points) {
        _points += points;
    }
    public float getPoints() {
        return _points;
    }
    
    public int getIndex() {
        return _index;
    }
    
    public ArrayList<Integer> getPath() {
        TreeNode currentNode = _parent;
        ArrayList<Integer> indexs = new ArrayList<Integer>();
        indexs.add(_index);
        while(currentNode._parent != null && currentNode._parent._parent != null) {
            indexs.add(currentNode._index);
            currentNode = currentNode._parent;
        }
        Collections.reverse(indexs);
        return indexs;
    }
    
    @Override
    public String toString() {
        return Integer.toString(_index);
    }
}
