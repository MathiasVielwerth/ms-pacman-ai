package maig.mvie.genetic;

import java.util.*;

/**
 * Created by mvie on 04-09-2014.
 */
public abstract class GeneticAlgorithm {
    static int POPULATION_SIZE = 100;

    private SortedMap<Float, Gene> population;
    private Phenotype base;

    public GeneticAlgorithm(Phenotype base) {
        // Generate random population
        this.base = base;
    }

    public void init() {
        ArrayList<Gene> firstPopulation = new ArrayList<Gene>();
        for(int i = 0; i < POPULATION_SIZE; i++) {
            firstPopulation.add(base.createRandomGene());
        }
        population = evaluateSelection(firstPopulation);
    }

    public Map.Entry<Float, Gene> getBest() {
        return population.entrySet().iterator().next();
    }

    private SortedMap<Float, Gene> evaluateSelection(ArrayList<Gene> selection) {
        // Run the fitness function on everyone
        TreeMap<Float, Gene> result = new TreeMap<Float, Gene>();
        for(Gene gene : selection) {
            float fitness = fitnessFunction(base, gene);
            result.put(fitness, gene);
        }

        return result.descendingMap();
    }

    public void runLifeCycle() {
        // Run one cycle of the algorithm
        ArrayList<Gene> selection = createSelection(population);
        ArrayList<Gene> children = recomburate(selection);
        SortedMap<Float, Gene> evaluatedChildren = evaluateSelection(children);

        TreeMap<Float, Gene> nextPopulation = new TreeMap<Float, Gene>();
        nextPopulation.putAll(getSubset(population, POPULATION_SIZE / 2));
        nextPopulation.putAll(getSubset(evaluatedChildren, POPULATION_SIZE - nextPopulation.size()));
        population = nextPopulation.descendingMap();
    }

    private <K extends Object, V extends Object> SortedMap<K, V> getSubset(SortedMap<K, V> origin, int size) {
        SortedMap<K, V> result = new TreeMap<K, V>();
        V[] genes = (V[])origin.values().toArray();
        K[] fitness = (K[])origin.keySet().toArray();
        for(int i = 0; i < size; i++) {
            result.put(fitness[i], genes[i]);
        }
        return result;
    }

    private ArrayList<Gene> recomburate(ArrayList<Gene> selection) {
        // Mate the selection from the population (aka, HAVE THE SEX)
        ArrayList<Gene> children = new ArrayList<Gene>();
        for(int i = 0; i < selection.size()-1; i = i+2) {
            Gene mom = selection.get(i);
            Gene dad = selection.get(i+1);
            children.add(dad.combine(mom));
            children.add(dad.combine(mom));
            children.add(dad.combine(mom));
        }
        return children;
    }

    private ArrayList<Gene> mutate(ArrayList<Gene> selection) {
        // Mutate the selection from the population
        return null;
    }

    private ArrayList<Gene> createSelection(SortedMap<Float, Gene> evaluatedSelection) {
        // Create selection based on their fitness
        ArrayList<Gene> result = new ArrayList<Gene>();
        for(Map.Entry<Float, Gene> kv : evaluatedSelection.entrySet()) {
            // TODO: MAKE THIS A PROPPER SELECTION!!;
            result.add(kv.getValue());
            if(result.size() > 50)
                break;
        }
        return result;
    }

    protected abstract float fitnessFunction(Phenotype base, Gene gene);
}
