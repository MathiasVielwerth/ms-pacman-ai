package maig.mvie.genetic;

/**
 * Created by mvie on 04-09-2014.
 */
public interface Phenotype<T> {
    public T acceptGene(Gene gene);
    public Gene createRandomGene();
}
