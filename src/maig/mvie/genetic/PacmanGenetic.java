package maig.mvie.genetic;

import pacman.controllers.Controller;
import pacman.entries.pacman.EvolutionPacMan;
import pacman.game.Constants;
import pacman.game.Game;

import java.util.EnumMap;
import java.util.Random;

import static pacman.game.Constants.DELAY;

/**
 * Created by mvie on 04-09-2014.
 */
public class PacmanGenetic extends GeneticAlgorithm {
    private int trials;
    Controller<EnumMap<Constants.GHOST,Constants.MOVE>> ghostController;

    public PacmanGenetic(Phenotype base, int trials, Controller<EnumMap<Constants.GHOST,Constants.MOVE>> ghostController) {
        super(base);
        this.trials = trials;
        this.ghostController = ghostController;
    }

    @Override
    protected float fitnessFunction(Phenotype base, Gene gene) {
        double avgScore=0;

        Random rnd=new Random(0);
        Game game;

        EvolutionPacMan pacman = (EvolutionPacMan) base.acceptGene(gene);

        for(int i=0;i<trials;i++)
        {
            game=new Game(rnd.nextLong());

            while(!game.gameOver())
            {
                game.advanceGame(pacman.getMove(game.copy(),System.currentTimeMillis()+DELAY),
                        ghostController.getMove(game.copy(),System.currentTimeMillis()+DELAY));
            }

            avgScore+=game.getScore();
            //System.out.println(i+"\t"+game.getScore());
        }

        //System.out.println(avgScore/trials);
        return (float)(avgScore/trials);
    }
}
