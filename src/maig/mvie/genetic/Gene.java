package maig.mvie.genetic;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by mvie on 04-09-2014.
 */
public class Gene {
    private Double[] chromosomes;

    public Gene(Double[] chromosomes) {
        this.chromosomes = chromosomes;
    }
    public int size() {
        return chromosomes.length;
    }
    public Double get(int i) {
        return chromosomes[i];
    }
    public Gene combine(Gene mom) {
        if(size() != mom.size())
            throw new IllegalArgumentException("Genes not the same size");

        ArrayList<Double> newChroms = new ArrayList <Double>();
        Random rnd = new Random();
        for(int i = 0; i < chromosomes.length; i++) {
            double dc = chromosomes[i];
            double mc = mom.chromosomes[i];
            double diff = dc - mc;
            double avg = diff * rnd.nextDouble();
            newChroms.add(avg + (dc > mc ? mc : dc));
        }
        Double[] tmp = new Double[newChroms.size()];
        return new Gene(newChroms.toArray(tmp));
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for(Double c : chromosomes) {
            str.append(c);
            str.append(" - ");
        }
        return str.toString();
    }
}
