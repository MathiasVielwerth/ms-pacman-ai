package pacman.entries.pacman;

import maig.mvie.genetic.Gene;
import maig.mvie.genetic.Phenotype;
import pacman.controllers.Controller;
import pacman.game.Constants.*;
import pacman.game.Game;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.util.*;

/**
 * Created by mvie on 03-09-2014.
 */

enum GHOST_MODE {
    ALL, EATABLE, DANGEROUS
}

public class EvolutionPacMan extends Controller<MOVE> implements Phenotype<EvolutionPacMan> {
    private double RUN_TRIGGER_DISTANCE = 5.0;
    private int ESCAPE_GHOST_DISTANCE = 69;
    private double CLOSEST_EATABLE_GHOST_DISTANCE = 212.56;
    private int PREDICT_GHOST_DISTANCE = 53;
    
    private int position;

    @Override
    public MOVE getMove(Game game, long timeDue) {
        position = game.getPacmanCurrentNodeIndex();
        
        Map.Entry<GHOST, Double> closest = closestGhost(game, GHOST_MODE.DANGEROUS);
        Map.Entry<GHOST, Double> closestEatable = closestGhost(game, GHOST_MODE.EATABLE);

        if(closest != null && closest.getValue() < RUN_TRIGGER_DISTANCE) {
            //System.out.println("------- ESCAAAAPE! ----------");
            return getEscapeRoute(game, closest.getKey(), ESCAPE_GHOST_DISTANCE);
        } else  if(closestEatable != null && closestEatable.getValue() < CLOSEST_EATABLE_GHOST_DISTANCE) {
            //System.out.println("------- Time to eat!---------");
            return towards(game, closestEatable.getKey());
            
        }
        //System.out.println("-------- Haps haps!----------");
        //return closestPill(game); //TODO: Something more advanced here
        return nextPill(game, PREDICT_GHOST_DISTANCE);
    }

    public MOVE getEscapeRoute(Game game, GHOST closest, int considerationDistance) {
        MOVE move = MOVE.NEUTRAL;
        
        ArrayList<MOVE> counterMoves = getCounterMoves(game, considerationDistance);

        if(counterMoves.size() > 0) {
            MOVE pillMove = nextPill(game, considerationDistance);
            move = counterMoves.contains(pillMove) ? pillMove : counterMoves.get(0);
            //System.out.println(counterMoves + " - " + move);
        } else {
            //MOVE powerPillMove = closestPowerPill(game);
            //MOVE awayMove = away(game, closest);
            //move = powerPillMove == MOVE.NEUTRAL || towards(game, closest) == powerPillMove ? awayMove : powerPillMove;
            
            move = away(game, closest);
            //System.out.println("We're dead! - " + move);
            //TODO: Look for closest intersect and run again
        }

        return move;
    }
    
    public MOVE closestPowerPill(Game game) {
        MOVE next = MOVE.NEUTRAL;
        double distance = Double.POSITIVE_INFINITY;
        for(int PPindex : game.getActivePowerPillsIndices()) {
            double dist = game.getDistance(position, PPindex, DM.PATH);
            if(dist < distance) {
                distance = dist;
                next = game.getNextMoveTowardsTarget(position, PPindex, DM.PATH);
            }
        }
        return next;
    }
    
    public ArrayList<MOVE> getCounterMoves(Game game, int considerationDistance) {
        ArrayList<MOVE> counterMoves = new ArrayList<MOVE>();
        counterMoves.add(MOVE.DOWN);
        counterMoves.add(MOVE.RIGHT);
        counterMoves.add(MOVE.LEFT);
        counterMoves.add(MOVE.UP);
        
        for(GHOST g : GHOST.values()) {
            int[] pathIndexs = game.getShortestPath(game.getGhostCurrentNodeIndex(g), position, game.getGhostLastMoveMade(g));
            if(pathIndexs.length != 0 && pathIndexs.length < considerationDistance) {
                int lastIndex = pathIndexs.length > 5 ? pathIndexs[pathIndexs.length - 5] : 0;
                if(lastIndex == 0) {
                    MOVE remove = towards(game, g);
                    //System.out.println("Errrr ...." + g + " removes " + remove);
                    counterMoves.remove(remove);
                    continue;
                }
                MOVE remove = towards(game, lastIndex);
                //System.out.println(g + " removes " + remove + " considering " + pathIndexs.length + " moves");
                counterMoves.remove(remove);
            }
        }
        
        List<MOVE> possible = Arrays.asList(game.getPossibleMoves(position));
        counterMoves.retainAll(possible);
        return counterMoves;
    }

    public MOVE towards(Game game, GHOST ghost, MOVE lastMove) {
        return towards(game, game.getGhostCurrentNodeIndex(ghost), lastMove);
    }
    public MOVE towards(Game game, int index, MOVE lastMove) {
        return game.getNextMoveTowardsTarget(position, index, lastMove, DM.PATH);
    }
    public MOVE towards(Game game, GHOST ghost) {
        return towards(game, game.getGhostCurrentNodeIndex(ghost));
    }
    public MOVE towards(Game game, int index) {
        return game.getNextMoveTowardsTarget(position, index, DM.PATH);
    }
    public MOVE away(Game game, GHOST ghost) {
        return game.getNextMoveAwayFromTarget(position, game.getGhostCurrentNodeIndex(ghost), DM.PATH);
    }

    public HashMap<GHOST, Double> distToGhosts(Game game, GHOST_MODE GM) {
        HashMap<GHOST, Double> dists = new HashMap<GHOST, Double>();
        for(GHOST g : GHOST.values()) {
            if(GM == GHOST_MODE.EATABLE && !game.isGhostEdible(g))
                continue;
            if(GM == GHOST_MODE.DANGEROUS && game.isGhostEdible(g))
                continue;
            double ghostDist = game.getDistance(position, game.getGhostCurrentNodeIndex(g), DM.PATH);
            if(ghostDist > 0)
                dists.put(g, ghostDist);
        }
        return dists;
    }

    public Map.Entry<GHOST, Double> closestGhost(Game game) { return closestGhost(game, GHOST_MODE.ALL);}
    public Map.Entry<GHOST, Double> closestGhost(Game game, GHOST_MODE GM) {
        HashMap<GHOST, Double> dists = distToGhosts(game, GM);

        Map.Entry<GHOST, Double> closest = null;
        double dist = Double.POSITIVE_INFINITY;
        for(Map.Entry<GHOST, Double> kv : dists.entrySet()) {
            if(kv.getValue() < dist) {
                dist = kv.getValue();
                closest = kv;
            }
        }
        return closest;
    }
    
    public MOVE nextPill(Game game, int ghostDistance) {
        ArrayList<MOVE> counterMoves = getCounterMoves(game, ghostDistance);
        
        //System.out.println(counterMoves);
        MOVE next = null;
        if(counterMoves.size() > 0) {
            next = closestPill(game, counterMoves);
        } else {
            next = closestPill(game);
        }
        //System.out.println("Move for " + next);
        return next;
    }

    public MOVE closestPill(Game game) { return closestPill(game, new ArrayList<MOVE>()); }
    public MOVE closestPill(Game game, ArrayList<MOVE> allowedDirections) {
        int[] pills = game.getActivePillsIndices();

        HashMap<GHOST, Double> dists = distToGhosts(game, GHOST_MODE.DANGEROUS);

        int targetIndex = 0;
        double distance = Double.POSITIVE_INFINITY;
        MOVE targetDirection = MOVE.NEUTRAL;
        for(int i = 0; i < pills.length; i++) {
            /*double distToTarget = allowedDirections.size() > 0? game.getDistance(position, pills[i], DM.PATH) 
                    : game.getDistance(position, pills[i], DM.PATH);*/
            double distToTarget = game.getDistance(position, pills[i], DM.PATH);
            MOVE m = towards(game, pills[i]);
            if((allowedDirections.size() == 0 || allowedDirections.contains(m)) && distToTarget < distance) {
                targetIndex = pills[i];
                distance = distToTarget;
                targetDirection = m;
            }
        }

        return targetDirection == MOVE.NEUTRAL && allowedDirections.size() > 0 ? closestPill(game) : targetDirection;
        //return targetDirection;
    }

    @Override
    public EvolutionPacMan acceptGene(Gene gene) {
        EvolutionPacMan next = new EvolutionPacMan();
        next.RUN_TRIGGER_DISTANCE = gene.get(0);
        next.ESCAPE_GHOST_DISTANCE = (int)Math.round(gene.get(1));
        next.CLOSEST_EATABLE_GHOST_DISTANCE = gene.get(2);
        next.PREDICT_GHOST_DISTANCE = (int)Math.round(gene.get(3));
        return next;
    }

    @Override
    public Gene createRandomGene() {
        Random rnd = new Random();
        Double[] chroms = new Double[4];

        chroms[0] = rnd.nextDouble() * 1000;
        chroms[1] = rnd.nextDouble() * 1000;
        chroms[2] = rnd.nextDouble() * 1000;
        chroms[3] = rnd.nextDouble() * 1000;

        return new Gene(chroms);
    }
}
