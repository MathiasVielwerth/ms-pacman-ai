/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pacman.entries.pacman;

import java.util.ArrayList;
import maig.mvie.treesearch.TreeSearch;
import pacman.controllers.Controller;
import pacman.game.Constants;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;
import pacman.game.Game;

/**
 *
 * @author mvie
 */
public class TreeSearchPacMan extends Controller<MOVE> {
    private Game _game;
    private int _lastBest;

    @Override
    public MOVE getMove(Game game, long timeDue) {
        _game = game;
        int position = game.getPacmanCurrentNodeIndex();
        
        //System.out.println("------- NEXT MOVE --------");
        //System.out.println("Im at " + _position);
        
        TreeSearch tree = new TreeSearch(game, 60, _lastBest);
        tree.search(position);
        ArrayList<Integer> path = tree.getRichestPath();
        int bestNext;
        if(path == null) {
            //System.out.println("------------- GOING FOR PILL!!!! -------------");
            bestNext = closestPillIndex();
        } else
            bestNext = path.get(0);

        _lastBest = bestNext;
        
        //System.out.println(path);
        
        MOVE nextMove = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), bestNext, Constants.DM.PATH);
        
        //System.out.println("Choosing " + nextMove);
        
        return nextMove;
    }

    private int closestPillIndex() {
        int[] pills = _game.getActivePillsIndices();

        int targetIndex = 0;
        int dist = Integer.MAX_VALUE;
        for(int pill : pills) {
            int pillDist = _game.getShortestPathDistance(_game.getPacmanCurrentNodeIndex(), pill);
            if(pillDist < dist) {
                dist = pillDist;
                targetIndex = pill;
            }
        }

        return targetIndex;
    }
}
